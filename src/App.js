import ImageTogel from "./component/imageTogel";
import "./App.css"

import Tom from './img/11.jpg'
import Hardy from './img/1.jpg'
import Clian from './img/2.jpg'
import Morphy from './img/22.jpg'

const App = () => {

  return (
    <div className="App">
      <ImageTogel colorful={Tom} blackAndWhite={Hardy} />
      <ImageTogel colorful={Clian} blackAndWhite={Morphy} />
    </div>
  )
}

export default App