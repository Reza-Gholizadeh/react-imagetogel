import React, {useRef} from "react";
import "./style.css"
const ImageTogel = ({colorful, blackAndWhite}) => {

  const imageRef = useRef(null)

  return (
     <img
     onMouseOver={() => {
      imageRef.current.src = colorful
     }}
     onMouseOut={() => {
       imageRef.current.src = blackAndWhite
     }}
      src={blackAndWhite}
      alt="img"
      ref={imageRef}
     />
  )
}

export default ImageTogel